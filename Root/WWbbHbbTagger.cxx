#include "WWbbTester/WWbbHbbTagger.h"

//std/stl
#include <iostream>
#include <vector>
using namespace std;

namespace wwbb
{
//_____________________________________________________________________________
WWbbHbbTagger::WWbbHbbTagger(SusyNtTools* tool) :
    m_nt_tools(tool)
{
    cout << "WWbbHbbTagger    Initializing..." << endl;
}
//_____________________________________________________________________________
WWbbHbbTagger::~WWbbHbbTagger()
{
    cout << "WWbbHbbTagger    Goodbye!" << endl;
}
//_____________________________________________________________________________
string WWbbHbbTagger::hbb_strategy_string(HbbStrategy strategy)
{
    string out = "HbbStrategyInvalid";
    switch(strategy) {
        case HbbStrategy::PtOrderedB : out = "PtOrderedB"; break;
        case HbbStrategy::BScoreOrdered : out = "BScoreOrdered"; break;
        case HbbStrategy::HbbStrategyInvalid : out = "HbbStrategyInvalid"; break;
    } // swtich
    return out;
}
//_____________________________________________________________________________
vector<Susy::Jet*> WWbbHbbTagger::get_jet_pair(wwbb::HbbStrategy strategy,
                                        vector<Susy::Jet*> jets, int btag_wp)
{
    vector<Susy::Jet*> out;
    switch(strategy) {
        case PtOrderedB :
            {
                out = get_pt_ordered_bjets(jets, btag_wp);
            }
            break;
        case BScoreOrdered :
            {
                out = get_btag_score_ordered_jets(jets, btag_wp);
            }
            break;
        case HbbStrategyInvalid :
            {
                cout << "WWbbHbbTagger::get_jet_pair    ERROR Invalid HbbStrategy provided" << endl;
                exit(1);
            }
    } // switch
    return out;
}
//_____________________________________________________________________________
vector<Susy::Jet*> WWbbHbbTagger::get_pt_ordered_bjets(vector<Susy::Jet*> jets, int wp)
{
    vector<Susy::Jet*> out;
    for(auto & j : jets) {
        if(tools()->jetSelector().isBMod(j, wp)) out.push_back(j);
    }
    std::sort(out.begin(), out.end(), comparePt);
    return out;
}
//_____________________________________________________________________________
vector<Susy::Jet*> WWbbHbbTagger::get_btag_score_ordered_jets(vector<Susy::Jet*> jets, int wp)
{
    vector<Susy::Jet*> out;

    vector<Susy::Jet*> bjets;
    for(auto & j : jets) {
        if(tools()->jetSelector().isBMod(j,wp)) bjets.push_back(j);
    }
    std::vector< std::pair<int,float>> mv2_scores_by_idx;
    for(int i = 0; i < (int)bjets.size(); i++) {
        mv2_scores_by_idx.push_back( std::pair<int, float>(i, bjets.at(i)->mv2c10));
    }
    std::sort(mv2_scores_by_idx.begin(), mv2_scores_by_idx.end(), mv2_sorter());

    for(auto v : mv2_scores_by_idx) {
        out.push_back( bjets.at(v.first) );
    }
    return out;
}
//_____________________________________________________________________________
bool WWbbHbbTagger::jet_is_truth_matched_to_Hbb(const Susy::Jet* jet)
{
    return (jet->jet_is_HbbMatched);
}
//_____________________________________________________________________________

} // namespace wwbb
