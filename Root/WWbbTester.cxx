#include "WWbbTester/WWbbTester.h"

// SusyNtuple
#include "SusyNtuple/KinematicTools.h"
#include "SusyNtuple/SusyDefs.h"
using namespace Susy; // everything in SusyNtuple is in this namespace

//ROOT

// std/stl
#include <iomanip> // setw
#include <iostream>
#include <string>
#include <sstream> // stringstream, ostringstream
using namespace std;

//////////////////////////////////////////////////////////////////////////////
WWbbTester::WWbbTester() :
    m_dbg(0),
    m_do_hbb(false),
    m_do_trig(false),
    m_input_chain(nullptr),
    m_mc_weight(1.0),
    m_trigger_tool(nullptr)
{
}
//////////////////////////////////////////////////////////////////////////////
void WWbbTester::Begin(TTree* /*tree*/)
{
    // call base class' Begin method
    SusyNtAna::Begin(0);
    if(dbg()) cout << "WWbbTester::Begin" << endl;

    return;
}
//////////////////////////////////////////////////////////////////////////////
void WWbbTester::Terminate()
{
    // close SusyNtAna and print timers
    SusyNtAna::Terminate();

    if(do_trig()) {
        // save trigger strategy items
        m_rfile_trig_strat->cd();
        for(int istrat = 0; istrat < wwbb::TriggerStrategy::StrategyInvalid; istrat++) {
            m_trig_strat_histos_lead_num[istrat]->Write();
            m_trig_strat_histos_lead_den[istrat]->Write();
            m_trig_strat_histos_sub_num[istrat]->Write();
            m_trig_strat_histos_sub_den[istrat]->Write();
        } // istrat
        m_rfile_trig_strat->Write();

        cout << "============================================" << endl;
        cout << "Total Number: " << n_total << endl;
        cout << "  > fraction passing OR  : " << ((float)n_pass_or / (float)n_total) << endl;
        cout << "  > fraction passing DIL : " << ((float)n_pass_dil / (float)n_total) << endl;
        cout << "============================================" << endl;
    }
    if(do_hbb()) {
        m_rfile_hbb_strat->cd();
        for(int istrat = 0; istrat < wwbb::HbbStrategy::HbbStrategyInvalid; istrat++) {
            h_hbb_strat_histos_mbb[istrat]->Write();
            h_hbb_strat_histos_mbb_window[istrat]->Write();
            h_hbb_strat_histos_mbb_matched[istrat]->Write();
            h_hbb_strat_histos_mbb_window_matched[istrat]->Write();
        }
        m_rfile_hbb_strat->Write();
    }

    return;
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void WWbbTester::Init(TTree* tree)
{
    cout << "WWbbTester::Init" << endl;
    SusyNtAna::Init(tree);


    if(do_trig()) {
        m_rfile_trig_strat = new TFile("wwbb_trig_strat.root", "RECREATE");

        n_pass_or = 0;
        n_pass_dil = 0;
        n_total = 0;

        m_trigger_tool = new wwbb::WWbbTriggerTool(&nttools());
        stringstream hname;
        stringstream htitle;
        for(int istrat = 0; istrat < wwbb::TriggerStrategy::StrategyInvalid; istrat++) {
            hname.str("");
            hname << "h_num_";
            hname << trigger_tool()->trigger_strategy_string((wwbb::TriggerStrategy)istrat);
            hname << "_lead";

            Double_t xbins[20] = {8.,9.,10., 11., 12.,13.,14.,15., 16., 17.,18.,19., 20., 22., 24.,26.,28.,30.,40.,70.};

            htitle.str("");
            htitle << trigger_tool()->trigger_strategy_string((wwbb::TriggerStrategy)istrat);
            htitle << ";Lead lepton p_{T} [GeV];#epsilon";
            m_trig_strat_histos_lead_num[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 19, xbins);
            //m_trig_strat_histos_lead_num[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 22, 6, 50);
            m_trig_strat_histos_lead_num[istrat]->Sumw2();

            hname.str("");
            hname << "h_den_";
            hname << trigger_tool()->trigger_strategy_string((wwbb::TriggerStrategy)istrat);
            hname << "_lead";
            m_trig_strat_histos_lead_den[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 19, xbins);
            //m_trig_strat_histos_lead_den[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 22, 6, 50);
            m_trig_strat_histos_lead_den[istrat]->Sumw2();

            hname.str("");
            htitle.str("");
            hname << "h_num_";
            hname << trigger_tool()->trigger_strategy_string((wwbb::TriggerStrategy)istrat);
            hname << "_sub";

            htitle.str("");
            htitle << trigger_tool()->trigger_strategy_string((wwbb::TriggerStrategy)istrat);
            htitle << ";Lead lepton p_{T} [GeV];#epsilon";
            m_trig_strat_histos_sub_num[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 19, xbins);
//            m_trig_strat_histos_sub_num[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 22, 6, 50);
            m_trig_strat_histos_sub_num[istrat]->Sumw2();

            hname.str("");
            hname << "h_den_";
            hname << trigger_tool()->trigger_strategy_string((wwbb::TriggerStrategy)istrat);
            hname << "_sub";
            m_trig_strat_histos_sub_den[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 19, xbins);
//            m_trig_strat_histos_sub_den[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), 22, 6, 50);
            m_trig_strat_histos_sub_den[istrat]->Sumw2();
        } // istrat
    } // do_trig

    if(do_hbb()) {
        initialize_hbb_analysis();
    }

}
//////////////////////////////////////////////////////////////////////////////
void WWbbTester::initialize_hbb_analysis()
{
    m_hbb_tagger = new wwbb::WWbbHbbTagger(&nttools());
    
    stringstream hname;
    stringstream htitle;

    m_rfile_hbb_strat = new TFile("wwbb_hbb_strat.root", "RECREATE");

    int n_bins = 40;
    int x_low = 0;
    int x_high = 200;
    for(int istrat = 0; istrat < wwbb::HbbStrategy::HbbStrategyInvalid; istrat++) {
            hname.str("");
            htitle.str("");

            hname << "h_hbb_";
            hname << hbb_tagger_tool()->hbb_strategy_string((wwbb::HbbStrategy)istrat);
            hname << "_mbb";

            htitle << ";m_{bb} (m_{jj}) [GeV];a.u.";
            h_hbb_strat_histos_mbb[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), n_bins, x_low, x_high); 
            h_hbb_strat_histos_mbb[istrat]->Sumw2();

            hname.str("");
            hname << "h_hbb_";
            hname << hbb_tagger_tool()->hbb_strategy_string((wwbb::HbbStrategy)istrat);
            hname << "_window";
            h_hbb_strat_histos_mbb_window[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), n_bins, x_low, x_high);
            h_hbb_strat_histos_mbb_window[istrat]->Sumw2();

            hname.str("");
            hname << hbb_tagger_tool()->hbb_strategy_string((wwbb::HbbStrategy)istrat);
            hname << "_matched";
            h_hbb_strat_histos_mbb_matched[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), n_bins, x_low, x_high);
            h_hbb_strat_histos_mbb_matched[istrat]->Sumw2();

            hname.str("");
            hname << hbb_tagger_tool()->hbb_strategy_string((wwbb::HbbStrategy)istrat);
            hname << "_window_matched";
            h_hbb_strat_histos_mbb_window_matched[istrat] = new TH1F(hname.str().c_str(), htitle.str().c_str(), n_bins, x_low, x_high);
            h_hbb_strat_histos_mbb_window_matched[istrat]->Sumw2();
            
     } // istrat
}
//////////////////////////////////////////////////////////////////////////////
Bool_t WWbbTester::Process(Long64_t entry)
{

    // calling "GetEntry" loads into memory the susyNt class objects for this event
    GetEntry(entry);
    SusyNtAna::clearObjects(); // clear the previous event's objects

    // increment the chain entry (c.f. SusyNtuple/SusyNtAna.h)
    m_chainEntry++;

    // evt() provides pointer to the SusyNt::Event class object for this event
    int run_number = nt.evt()->run;
    int event_number = nt.evt()->eventNumber;

    if(dbg() || m_chainEntry%1000==0) {
        cout << "WWbbTester::Process    **** Processing entry " << setw(6) << m_chainEntry
                << "  run " << run_number << "  event " << event_number << " **** " << endl;
    }

    // SusyNtAna::selectObject fills the baseline and signal objects
    // for the given AnalysisType
    // m_preX    = objects before any selection (as they are in susyNt)
    // m_baseX   = objects with the Analysis' baseline selection AND overlap removal applied
    // m_signalX = objects with the Analysis' signal selection applied (and baseline AND overlap removal)
    SusyNtAna::selectObjects();

    // get the MC weight using the inherited MCWeighter object
    // (c.f. SusyNtuple/MCWeighter.h)
    if(nt.evt()->isMC) {
        float lumi = 100000; // normalize the MC to 100 fb-1
        m_mc_weight = SusyNtAna::mcWeighter().getMCWeight(nt.evt(), lumi, NtSys::NOM);
    }
    else {
        m_mc_weight = 1.; // don't re-weight data
    }

    // check that the event passes the standard ATLAS event cleaning cuts
    if(!passEventCleaning(m_preMuons, m_baseMuons, m_baseJets)) return false;

    if(do_trig())
        run_trigger_strategy_test();
    if(do_hbb())
        run_hbb_strategy_test();

    return kTRUE;
}
//////////////////////////////////////////////////////////////////////////////
bool WWbbTester::passEventCleaning(const MuonVector& preMuons, const MuonVector& baseMuons,
            const JetVector& baseJets)
{
    int flags = nt.evt()->cutFlags[NtSys::NOM];

    if(!nttools().passGRL(flags))           return false;

    if(!nttools().passLarErr(flags))        return false;

    if(!nttools().passTileErr(flags))       return false;

    if(!nttools().passTTC(flags))           return false;

    if(!nttools().passSCTErr(flags))        return false;

    if(!nttools().passGoodVtx(flags))       return false;


    ///////////////////////////////////////////////////////
    // for bad muon, cosmic moun, and jet cleaning the
    // cuts depend on the baseline object defintion
    // (and in thec ase of the cosmic muon cut, it also
    // depends on the analysis' overlap removal
    // procedure) -- so we do not use the cutFlags but
    // rather use the objects that have passed the various
    // analysis selections to do the checks
    ///////////////////////////////////////////////////////
    if(!nttools().passBadMuon(preMuons))    return false;

    if(!nttools().passCosmicMuon(baseMuons)) return false;

    if(!nttools().passJetCleaning(baseJets)) return false;

    return true;
}
//////////////////////////////////////////////////////////////////////////////
void WWbbTester::run_trigger_strategy_test()
{
    auto leptons = m_signalLeptons;

    // enforce EXACTLY two signal level leptons
    if(leptons.size() != 2) return;

   // double ev_weight = m_mc_weight;

    n_total++;
    for(int istrat = 0; istrat < wwbb::TriggerStrategy::StrategyInvalid; istrat++) {
        m_trig_strat_histos_lead_den[istrat]->Fill(leptons.at(0)->Pt());//, ev_weight);
        m_trig_strat_histos_sub_den[istrat]->Fill(leptons.at(1)->Pt());//, ev_weight);
    }


    if(trigger_tool()->pass_trigger_strategy(wwbb::TriggerStrategy::TightSingleORDilepton,
                                                    nt.evt(), leptons.at(0), leptons.at(1)))
    {
        m_trig_strat_histos_lead_num[wwbb::TriggerStrategy::TightSingleORDilepton]->Fill(leptons.at(0)->Pt());//, ev_weight);
        m_trig_strat_histos_sub_num[wwbb::TriggerStrategy::TightSingleORDilepton]->Fill(leptons.at(1)->Pt());//, ev_weight);
        n_pass_or++;
    }
    if(trigger_tool()->pass_trigger_strategy(wwbb::TriggerStrategy::Dilepton,
                                                    nt.evt(), leptons.at(0), leptons.at(1)))
    {
        m_trig_strat_histos_lead_num[wwbb::TriggerStrategy::Dilepton]->Fill(leptons.at(0)->Pt());//, ev_weight);
        m_trig_strat_histos_sub_num [wwbb::TriggerStrategy::Dilepton]->Fill(leptons.at(1)->Pt());//, ev_weight);
        n_pass_dil++;
    }

}
//////////////////////////////////////////////////////////////////////////////
void WWbbTester::run_hbb_strategy_test()
{
    auto jets = m_signalJets;
    if(jets.size() < 2) return;

    for(int istrat = 0; istrat < wwbb::HbbStrategy::HbbStrategyInvalid; istrat++) {
        int wp = 70;
        if(istrat == wwbb::HbbStrategy::BScoreOrdered) wp = 70;

        auto jet_pair = hbb_tagger_tool()->get_jet_pair((wwbb::HbbStrategy)istrat, jets, wp);
        if(jet_pair.size()<2) continue;
        float mjj = (*jet_pair.at(0) + *jet_pair.at(1)).M();
        h_hbb_strat_histos_mbb[istrat]->Fill(mjj);

        // CMS window
        bool inside_window = mjj>75 && mjj<140;
        if(inside_window) {
            h_hbb_strat_histos_mbb_window[istrat]->Fill(mjj);
        }

        bool first_matched = hbb_tagger_tool()->jet_is_truth_matched_to_Hbb(jet_pair.at(0));
        bool second_matched = hbb_tagger_tool()->jet_is_truth_matched_to_Hbb(jet_pair.at(1));
        bool hbb_matched = first_matched && second_matched;
        if(hbb_matched) {
            h_hbb_strat_histos_mbb_matched[istrat]->Fill(mjj);
        }

        if(inside_window && hbb_matched) {
            h_hbb_strat_histos_mbb_window_matched[istrat]->Fill(mjj);
        }
    } // istrat
}
