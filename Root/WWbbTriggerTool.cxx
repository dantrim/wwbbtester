#include "WWbbTester/WWbbTriggerTool.h"

// SusyNtuple
//#include "SusyNtuple/SusyNtTools.h"
//#include "SusyNtuple/Event.h"

// std/stl
#include <iostream>
using namespace std;

namespace wwbb
{

//________________________________________________________
WWbbTriggerTool::WWbbTriggerTool(SusyNtTools* tool) :
    m_nt_tools(tool)
{
   cout << "WWbbTriggerTool    Initializing..." << endl;
}
//________________________________________________________
WWbbTriggerTool::~WWbbTriggerTool()
{
    cout << "WWbbTriggerTool    Goodbye!" << endl;

}
//________________________________________________________
std::string WWbbTriggerTool::trigger_strategy_string(TriggerStrategy strategy)
{
    string out = "TriggerStrategyInvalid";
    switch(strategy) {
        case TriggerStrategy::TightSingleORDilepton : out = "TightSingleORDilepton"; break;
        case TriggerStrategy::TightDilepton : out = "TightDilepton"; break;
        case TriggerStrategy::Dilepton : out = "Dilepton"; break;
        case TriggerStrategy::DileptonCMS : out = "DileptonCMS"; break;
        case TriggerStrategy::StrategyInvalid : out = "TriggerStrategyInvalid"; break;
    } // switch
    return out;
}
//________________________________________________________
bool WWbbTriggerTool::event_fired_trigger(const Susy::Event* event,
        string trigger_name)
{
    return this->tools()->triggerTool().passTrigger(event->trigBits, trigger_name);
}
//________________________________________________________
DileptonFlavor WWbbTriggerTool::get_dilepton_flavor(const Lepton* lep0,
    const Lepton* lep1)
{
    const Lepton* lead = nullptr;
    const Lepton* sub = nullptr;
    if(lep0->Pt() > lep1->Pt()) { lead = lep0; sub = lep1; }
    else { lead = lep1; sub = lep0; }

    if(lead->isEle() && sub->isEle()) {
        return DileptonFlavor::EE;
    }
    else if(lead->isMu() && sub->isMu()) {
        return DileptonFlavor::MM;
    }
    else if(lead->isEle() && sub->isMu()) {
        return DileptonFlavor::EM;
    }
    else if(lead->isMu() && sub->isEle()) {
        return DileptonFlavor::ME;
    }
    else {
        return DileptonFlavor::DileptonFlavorInvalid;
    }
    return DileptonFlavor::DileptonFlavorInvalid;
}
//________________________________________________________
bool WWbbTriggerTool::pass_trigger_strategy(TriggerStrategy strategy,
        const Event* event, const Lepton* lep0, const Lepton* lep1)
{
    bool passes = false;
    switch(strategy) {
        case TightSingleORDilepton :
            passes = this->test_strategy_TightSingleORDilepton(event, lep0, lep1);
            break;
        case TightDilepton :
            passes = this->test_strategy_TightDilepton(event, lep0, lep1);
            break;
        case Dilepton :
            passes = this->test_strategy_Dilepton(event, lep0, lep1);
            break;
        case DileptonCMS :
            passes = this->test_strategy_DileptonCMS(event, lep0, lep1);
            break;
        case StrategyInvalid :
            cout << "WWbbTriggerTool::pass_trigger_strategy    "
                    "ERROR Invalid trigger strategy (" << strategy << ") provided" << endl;
            exit(1);
            break;
    } // swtich
    return passes;

}
//________________________________________________________
bool WWbbTriggerTool::test_strategy_TightSingleORDilepton(const Event* event,
    const Lepton* lep0, const Lepton* lep1)
{
    DileptonFlavor flavor = get_dilepton_flavor(lep0, lep1);
    const Lepton* lead = nullptr;
    const Lepton* sub = nullptr;
    if(lep0->Pt() > lep1->Pt()) { lead = lep0; sub = lep1; }
    else { lead = lep1; sub = lep0; }

    float pt0 = lead->Pt();
    float pt1 = sub->Pt();
    int year = event->treatAsYear;

    if(year == 2015) {
        // EE
        if(flavor == DileptonFlavor::EE) {
            if(pt0 >= 26 && this->event_fired_trigger(event, "HLT_e24_lhmedium_L1EM20VHI")) {
                return true;
            }
            else if( (pt0>=14 && pt1>=14) && this->event_fired_trigger(event, "HLT_2e12_lhloose_L12EM10VH")) {
                return true;
            }
            else {
                return false;
            }
        } // EE
        // MM
        else if(flavor == DileptonFlavor::MM) {
            if(pt0 >= 22 && this->event_fired_trigger(event, "HLT_mu20_iloose_L1MU15")) {
                return true;
            }
            else if( (pt0>=20 && pt1>=10) && this->event_fired_trigger(event, "HLT_mu18_mu8noL1")) {
                return true;
            }
            else {
                return false;
            }
        } // MM
        // EM
        else if(flavor == DileptonFlavor::EM) {
            if(pt0 >= 26 && this->event_fired_trigger(event, "HLT_e24_lhmedium_L1EM20VHI")) {
                return true;
            }
            else if( (pt0 >= 26 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_e24_lhmedium_L1EM20VHI_mu8noL1")) {
                return true;
            }
            else if( (pt0 >= 19 && pt1 >= 16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        }
        // ME
        else if(flavor == DileptonFlavor::ME) {
            if(pt0 >= 22 && this->event_fired_trigger(event, "HLT_mu20_iloose_L1MU15")) {
                return true;
            }
            else if( (pt0 >= 26 && pt1 >= 9) && this->event_fired_trigger(event, "HLT_e7_lhmedium_mu24")) {
                return true;
            }
            else if( (pt0 >= 19 && pt1 >= 16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            cout << "WWbbTriggerTool::test_strategy_TightSingleORDilepton    "
                "Received invalid DileptonFlavor!" << endl;
        }
    } // 2015
    else if(year == 2016) {
        // EE
        if(flavor == DileptonFlavor::EE) {
            if(pt0 >= 28 && this->event_fired_trigger(event, "HLT_e26_lhtight_nod0_ivarloose")) {
                return true;
            }
            else if( (pt0 >= 19 && pt1 >= 19) && this->event_fired_trigger(event, "HLT_2e17_lhvloose_nod0")) {
                return true;
            }
            else {
                return false;
            }
        } // EE
        // MM
        else if(flavor == DileptonFlavor::MM) {
            if( pt0 >= 28 && this->event_fired_trigger(event, "HLT_mu26_ivarmedium")) {
                return true;
            }
            else if( (pt0 >= 24 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_mu22_mu8noL1")) {
                return true;
            }
            else {
                return false;
            }
        } // MM
        // EM
        else if(flavor == DileptonFlavor::EM) {
            if( pt0 >= 28 && this->event_fired_trigger(event, "HLT_e26_lhtight_nod0_ivarloose")) {
                return true;
            }
            else if( (pt0 >= 28 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1")) {
                return true;
            }
            else if( (pt0 >= 19 && pt1 >= 16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        } // EM
        // ME
        else if(flavor == DileptonFlavor::ME) {
            if( pt0 >= 28 && this->event_fired_trigger(event, "HLT_mu26_ivarmedium")) {
                return true;
            }
            else if( (pt0 >= 26 && pt1 >= 9) && this->event_fired_trigger(event, "HLT_e7_lhmedium_nod0_mu24")) {
                return true;
            }
            else if( (pt0 >= 19 && pt1 >= 16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        } // ME
        else  {
            cout << "WWbbTriggerTool::test_strategy_TightSingleORDilepton    "
                "Received invalid DileptonFlavor!" << endl;
        }
    } // 2016

    // shouldn't get here
    return false;
}
//________________________________________________________
bool WWbbTriggerTool::test_strategy_TightDilepton(const Event* event,
    const Lepton* lep0, const Lepton* lep1)
{
    cout << "WWbbTriggerTool::test_strategy_TightDilepton    "
        "This is not yet implemented!" << endl;
    return true;
}
//________________________________________________________
bool WWbbTriggerTool::test_strategy_Dilepton(const Event* event,
    const Lepton* lep0, const Lepton* lep1)
{
    DileptonFlavor flavor = get_dilepton_flavor(lep0, lep1);
    const Lepton* lead = nullptr;
    const Lepton* sub = nullptr;
    if(lep0->Pt() > lep1->Pt()) { lead = lep0; sub = lep1; }
    else { lead = lep1; sub = lep0; }

    float pt0 = lead->Pt();
    float pt1 = sub->Pt();
    int year = event->treatAsYear;

    // 2015
    if(year == 2015) {
        // EE
        if(flavor == DileptonFlavor::EE) {
            if( (pt0 >= 14 && pt1 >= 14) && this->event_fired_trigger(event, "HLT_2e12_lhloose_L12EM10VH")) {
                return true;
            }
            else {
                return false;
            }
        } // EE
        // MM
        else if(flavor == DileptonFlavor::MM) {
            if( (pt0 >= 20 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_mu18_mu8noL1")) {
                return true;
            }
            else  {
                return false;
            }
        } // MM
        // EM
        else if(flavor == DileptonFlavor::EM) {
            if( (pt0 >= 26 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_e24_lhmedium_L1EM20VHI_mu8noL1")) {
                return true;
            }
            else if( (pt0>=19 && pt1>=16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        } // EM
        // ME
        else if(flavor == DileptonFlavor::ME) {
            if( (pt0 >= 26 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_e7_lhmedium_mu24")) {
                return true;
            }
            else if( (pt0 >= 19 && pt1 >= 16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        } // ME
        else {
            cout << "WWbbTriggerTool::test_strategy_TightSingleORDilepton    "
                "Received invalid DileptonFlavor!" << endl;
        }
    } // year == 2015
    else if(year == 2016) {
        // EE
        if(flavor == DileptonFlavor::EE) {
            if((pt0 >= 19 && pt1 >= 19) && this->event_fired_trigger(event, "HLT_2e17_lhvloose_nod0")) {
                return true;
            }
            else {
                return false;
            }
        } // EE
        // MM
        else if(flavor == DileptonFlavor::MM) {
            if((pt0 >= 24 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_mu22_mu8noL1")) {
                return true;
            }
            else {
                return false;
            }
        } // MM
        // EM
        else if(flavor == DileptonFlavor::EM) {
            if((pt0 >= 28 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1")) {
                return true;
            }
            else if((pt0>=19 && pt1>=16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        } // EM
        // ME
        else if(flavor == DileptonFlavor::ME) {
            if( (pt0 >= 26 && pt1 >= 10) && this->event_fired_trigger(event, "HLT_e7_lhmedium_nod0_mu24")) {
                return true;
            }
            else if( (pt0 >= 19 && pt1 >= 16) && this->event_fired_trigger(event, "HLT_e17_lhloose_mu14")) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            cout << "WWbbTriggerTool::test_strategy_TightSingleORDilepton    "
                "Received invalid DileptonFlavor!" << endl;
        }
    } // year == 2016



    // shouldn't get here
    return false;
}
//________________________________________________________
bool WWbbTriggerTool::test_strategy_DileptonCMS(const Event* event,
    const Lepton* lep0, const Lepton* lep1)
{


    return true;
}

} // namespace wwbb
