#ifndef SusyNtuple_WWbbTester_h
#define SusyNtuple_WWbbTester_h

//ROOT
#include "TTree.h"
#include "TChain.h"

//SusyNtuple
#include "SusyNtuple/SusyNtAna.h"
#include "SusyNtuple/SusyNtTools.h"

//Tester
#include "WWbbTester/WWbbTriggerTool.h"
#include "WWbbTester/WWbbHbbTagger.h"


//ROOT
#include "TFile.h"
#include "TH1F.h"

//std/stl
#include <fstream>

/////////////////////////////////////////////////////////////
//
// WWbbTester
// Class auto-generated with SusyNtuple/make_susy_skeleton on 2018-10-31 09:20
//
/////////////////////////////////////////////////////////////

// for TSelector analysis loopers processing susyNt you MUST inherit from SusyNtAna
// in order to pick up the susyNt class objects
class WWbbTester : public SusyNtAna
{

    public :
        WWbbTester();
        virtual ~WWbbTester() {};

        void set_debug(int dbg) { m_dbg = dbg; }
        int dbg() { return m_dbg; }

        void set_hbb(bool doit) { m_do_hbb = doit; }
        bool do_hbb() { return m_do_hbb; }
        void set_trig(bool doit) { m_do_trig = doit; }
        bool do_trig() { return m_do_trig; }

        void set_chain(TChain* chain) { m_input_chain = chain; }
        TChain* chain() { return m_input_chain; }

        ////////////////////////////////////////////
        // analysis methods
        ////////////////////////////////////////////

        // standard ATLAS event cleaning
        bool passEventCleaning(const MuonVector& preMuons, const MuonVector& baseMuons,
                const JetVector& baseJets);

        void initialize_hbb_analysis();

        void run_trigger_strategy_test();
        void run_hbb_strategy_test();

        // Tester
        wwbb::WWbbTriggerTool* trigger_tool() { return m_trigger_tool; }
        wwbb::WWbbHbbTagger* hbb_tagger_tool() { return m_hbb_tagger; }


        ////////////////////////////////////////////
        // TSelector methods override
        ////////////////////////////////////////////
        virtual void Begin(TTree* tree); // Begin is called before looping on entries
        virtual void Init(TTree* tree);
        virtual Bool_t Process(Long64_t entry); // Main event loop function called on each event
        virtual void Terminate(); // Terminate is called after looping has finished


    private :
        int m_dbg;
        bool m_do_hbb;
        bool m_do_trig;
        TChain* m_input_chain; // the TChain object we are processing
        float m_mc_weight;

        wwbb::WWbbTriggerTool* m_trigger_tool;
        wwbb::WWbbHbbTagger* m_hbb_tagger;

        // TRIGGER STRATEGY
        TFile* m_rfile_trig_strat;
        TH1F* m_trig_strat_histos_lead_num [ wwbb::TriggerStrategy::StrategyInvalid ];
        TH1F* m_trig_strat_histos_lead_den [ wwbb::TriggerStrategy::StrategyInvalid ];
        TH1F* m_trig_strat_histos_sub_num [ wwbb::TriggerStrategy::StrategyInvalid ];
        TH1F* m_trig_strat_histos_sub_den [ wwbb::TriggerStrategy::StrategyInvalid ];

        int n_pass_or;
        int n_pass_dil;
        int n_total;

        // HBB SRATEGY
        TFile* m_rfile_hbb_strat;
        TH1F* h_hbb_strat_histos_mbb [ wwbb::HbbStrategy::HbbStrategyInvalid ];
        TH1F* h_hbb_strat_histos_mbb_window [ wwbb::HbbStrategy::HbbStrategyInvalid ];
        TH1F* h_hbb_strat_histos_mbb_matched [ wwbb::HbbStrategy::HbbStrategyInvalid ];
        TH1F* h_hbb_strat_histos_mbb_window_matched [ wwbb::HbbStrategy::HbbStrategyInvalid ];



}; //class


#endif
