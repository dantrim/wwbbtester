#ifndef WWBB_HBBTAGGER_H
#define WWBB_HBBTAGGER_H

//SusyNtuple
#include "SusyNtuple/SusyNtTools.h"
#include "SusyNtuple/Jet.h"
#include "SusyNtuple/Event.h"
using namespace Susy;

//std/stl
#include <string>

namespace wwbb {

enum HbbStrategy
{
    PtOrderedB=0
    ,BScoreOrdered
    ,HbbStrategyInvalid
}; // enum HbbStrategy

struct mv2_sorter {
    bool operator()(const std::pair<int, float> & left, const std::pair<int, float> & right)
            { return left.second > right.second; }
};

class WWbbHbbTagger
{
    public :
        WWbbHbbTagger(SusyNtTools* tool);
        virtual ~WWbbHbbTagger();

        SusyNtTools* tools() { return this->m_nt_tools; }

        std::string hbb_strategy_string(HbbStrategy strategy);

        std::vector<Susy::Jet*> get_jet_pair(wwbb::HbbStrategy, std::vector<Susy::Jet*> jets,
                                int btag_wp = 77);
        std::vector<Susy::Jet*> get_pt_ordered_bjets(std::vector<Susy::Jet*> jets, int btag_wp = 77);
        std::vector<Susy::Jet*> get_btag_score_ordered_jets(std::vector<Susy::Jet*> jets, int btag_wp = 77);
        bool jet_is_truth_matched_to_Hbb(const Susy::Jet* jet);

    private :
        SusyNtTools* m_nt_tools;

}; // class WWbbHBbbTagger


} // namespace wwbb

#endif
