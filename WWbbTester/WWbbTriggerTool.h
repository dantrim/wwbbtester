#ifndef WWBB_TRIGGERTOOL_H
#define WWBB_TRIGGERTOOL_H

// SusyNtuple
//namespace Susy {
//    class SusyNtTools;
//    class Event;
//}
#include "SusyNtuple/SusyNtTools.h"
#include "SusyNtuple/Event.h"
using namespace Susy;

// std/stl
#include <string>

namespace wwbb {

enum TriggerStrategy
{
    TightSingleORDilepton=0
    ,TightDilepton
    ,Dilepton
    ,DileptonCMS
    ,StrategyInvalid
};

enum DileptonFlavor
{
    // this is pt-ordered
    EE=0
    ,MM
    ,EM
    ,ME
    ,DileptonFlavorInvalid
};

class WWbbTriggerTool
{

    public :
        WWbbTriggerTool(SusyNtTools* tool);
        virtual ~WWbbTriggerTool();

        SusyNtTools* tools() { return m_nt_tools; }

        bool event_fired_trigger(const Event* event, std::string trigger_name);

        bool pass_trigger_strategy(TriggerStrategy strategy,
            const Event* event, const Lepton* lep0, const Lepton* lep1);
        static std::string trigger_strategy_string(const TriggerStrategy strategy);

        static DileptonFlavor get_dilepton_flavor(const Lepton* lep0, const Lepton* lep1);

    private :
        SusyNtTools* m_nt_tools;

        bool test_strategy_TightSingleORDilepton(const Event* event, const Lepton* lep0, const Lepton* lep1);
        bool test_strategy_TightDilepton(const Event* event, const Lepton* lep0, const Lepton* lep1);
        bool test_strategy_Dilepton(const Event* event, const Lepton* lep0, const Lepton* lep1);
        bool test_strategy_DileptonCMS(const Event* event, const Lepton* lep0, const Lepton* lep1);


}; // class WWbbTriggerTool

} // namespace wwbb

#endif
